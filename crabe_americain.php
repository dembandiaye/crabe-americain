<?php

/**
 * Get the crabe's and chloromesteque's position.
 *
 * @param array $solMarin
 * @param int $m
 *
 * @return array
 */
function getCrabeAndChloroMestequePosition(array $solMarin, $m)
{
    $chloromestequePosition = $crabePosition = false;
    // Get Positions
    for ($row = 0; $row < $m; $row++) {
        if (false === $crabePosition) {
            $crabePosition = getElementPosition('{', $solMarin, $row);
        }
        if (false === $chloromestequePosition) {
            $chloromestequePosition = getElementPosition('$', $solMarin, $row);
        }

        if (is_array($chloromestequePosition) && is_array($crabePosition)) { // We No longer need to pursue.
            break;
        }
    }

    return [$crabePosition, $chloromestequePosition];
}

/**
 * Get the position of a given string in the matrix representing sol Marin.
 *
 * @param string $needle
 * @param array $solMarin
 * @param int $row
 *
 * @return bool|array
 */
function getElementPosition($needle, array $solMarin, $row)
{
    $pos = false;
    $find = array_search($needle, $solMarin[$row]);
    if (false !== $find) {
        $pos['row'] = $row;
        $pos['col'] = $find;
    }
    return $pos;

}

// Recuperer les dimensions du sol marin
list($m, $n) = array_map("intval", explode(" ", fgets(STDIN)));

echo "m = $m\n";
echo "n = $n\n";

$solMarin = [];
// Recuperer les valeurs du sol marin
for ($row = 0; $row < $m; $row++) {
    echo " Donnez les valeurs de la ligne $row:";
    $solMarin[$row] = str_split(fgets(STDIN));
}

// Recuperer les position du crabe et du chloromesteque
list($crabePosition, $chloromestequePosition) = getCrabeAndChloroMestequePosition($solMarin, $m);

if (!is_array($chloromestequePosition) || !is_array($crabePosition)) {
    throw new Exception('Invalid Sol Marin : { or $ may be missing');
}

$distance = abs($chloromestequePosition['row'] - $crabePosition['row']) + abs($chloromestequePosition['col'] - $crabePosition['col']);

echo "Finished And Result Is: $distance\n";